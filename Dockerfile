# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Dockerfile                                         :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/08/13 12:44:08 by lgorilla          #+#    #+#              #
#    Updated: 2020/10/29 15:29:01 by lgorilla         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

FROM debian:buster

RUN apt-get update && apt-get -y upgrade
RUN apt-get -y install php7.3-fpm php7.3-common php7.3-mysql php7.3-gmp php7.3-curl php7.3-intl php7.3-mbstring php7.3-xmlrpc php7.3-gd php7.3-xml php7.3-cli php7.3-zip php7.3-soap php7.3-imap
RUN apt-get -y install wget
RUN apt-get -y install nginx
RUN apt-get -y install mariadb-server

COPY srcs/start_server.sh ./
COPY srcs/server.conf ./tmp/
COPY srcs/php_conf.inc.php ./tmp/
COPY srcs/wp-config.php ./tmp/
COPY srcs/index.html ./tmp/
COPY srcs/change_index.sh ./

EXPOSE 80 443

CMD bash start_server.sh


