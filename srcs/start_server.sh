mkdir /var/www/localhost/
#рекурсивно меняем права всех файлов пользователя
chown -R www-data /var/www/*
chmod -R 755 /var/www/*

mv /tmp/server.conf /etc/nginx/sites-available/localhost
ln -s /etc/nginx/sites-available/localhost /etc/nginx/sites-enabled/localhost

#mv /tmp/index.html /var/www/localhost/

service mysql start
echo "CREATE DATABASE wordpress;" | mysql -u root --skip-password
echo "GRANT ALL PRIVILEGES ON wordpress.* TO 'root'@'localhost' WITH GRANT OPTION;" | mysql -u root --skip-password
echo "update mysql.user set plugin='mysql_native_password' where user='root';" | mysql -u root --skip-password
echo "FLUSH PRIVILEGES;" | mysql -u root --skip-password

mkdir /var/www/phpmyadmin
wget https://files.phpmyadmin.net/phpMyAdmin/4.9.0.1/phpMyAdmin-4.9.0.1-all-languages.tar.gz
tar -xvf phpMyAdmin-4.9.0.1-all-languages.tar.gz --strip-components 1 -C /var/www/phpmyadmin
mv /tmp/php_conf.inc.php /var/www/phpmyadmin/config.inc.php

cd /tmp/
wget -c https://wordpress.org/latest.tar.gz
tar -xvzf /tmp/latest.tar.gz
mv wordpress/ /var/www/
mv /tmp/wp-config.php /var/www/wordpress


openssl req -x509 -nodes -days 365 -newkey rsa:2048 -subj '/C=RU/ST=MS/L=MS/0=21/CN=lgorilla' \
-keyout /etc/ssl/certs/localhost.key -out /etc/ssl/certs/localhost.crt

service php7.3-fpm start
service nginx start
bash

